"""imports a reddit user's .json file and can parse for post/comment info."""

import json

import requests


class RedditUser(object):

    def __init__(self, username):
        self.username = username
        self.profile = None
        self.karma = None

    def json_import(self):
        """imports the profile .json data from the Reddit username input.
           accounts for HTTP errors 404 and 429.
           Any other HTTP error is saved to log.txt.
        """
        try:
            with requests.get("http://api.reddit.com/user/{}?limit=100".format(self.username),
                              headers={"user-agent": "karmacompbot"}) as r:
                r.raise_for_status()
                return json.loads(r.text)
        except requests.exceptions.HTTPError as e:
            if "404" in str(e):
                raise ConnectionError("User not found")
            elif "429" in str(e):
                raise ConnectionRefusedError("Too many requests")

    def json_import_local(self):
        """Imports the profile .json data from a local .json file"""
        with open('{}.json'.format(self.username)) as json_data:
            d = json.load(json_data)
        return d

    def recent_karma(self, submission_type):
        """returns the karma for the most recent link posted"""
        # Raise error if instance does not have profile.json imported
        if self.profile is None:
            raise AttributeError("no profile attribute found for RedditUser instance {}".format(self.username))

        # assign correct api identifier code for submission type
        if submission_type.lower() in {"comment", "comments"}:
            vector = "t1"
        elif submission_type.lower() in {"link", "links"}:
            vector = "t3"
        else:
            raise TypeError("Submission type should be either 'comment' or 'link'")

        # Iterate over json until submission of correct type is found
        i = 0
        kind = self.profile['data']['children'][i]['kind']
        while kind != vector:
            kind = self.profile['data']['children'][i]['kind']
            i += 1
            if i >= 99:
                return None
            continue
        return self.profile['data']['children'][i]['data']['score']
