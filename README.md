# reddit-analyser

## What is it?
`reddit_user.py` allows you to easily retrieve the profile json data of a reddit user's
last 100 posts/comments, and also extract the karma for the most recent link or comment posted
using built-in functions.
For an example which compares karma between 2 users see `karma.py`

## How do I use it?
* Instantiate a new `RedditUser` object by passing in the username of the profile
you want to analyze.

* Import the user's profile.json with the `json_import()` method to obtain the json file through Reddit's API, or the
`json_import_local()` method to supply a local json file. `json_import()` will raise a `ConnectionRefusedError` for HTTP
429 errors, and a `ConnectionError` for a 404 error.

* To obtain the most recent karma from a comment or link call the `recent_karma()` function with either "comment" or
"link" as the argument.

## Have an idea on how to improve this project?
Feel free to open an issue, or better yet a pull request! 
