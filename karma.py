"""Compares Karma received between 2 users' most recent posts"""

import sys

from reddit_user import RedditUser


def main():
    # Define users and submission type
    user1 = make_user(1)
    user2 = make_user(2)

    # Define submission type
    global submission_type
    submission_type = None
    while submission_type not in {"comment", "comments", "link", "links"}:
        submission_type = input("Please enter either 'comment', or 'link': ")

    # Get karma for most recent submission type
    user1.karma = user1.recent_karma(submission_type)
    user2.karma = user2.recent_karma(submission_type)

    # Determine which user's most recent post/comment has the highest karma.
    highest_karma(user1, user2)


def make_user(user_number):
    """Instantiates and handles http 404 and 429 errors"""
    while True:
        username = input("user {}: ".format(user_number))
        user = RedditUser(username)
        try:
            user.profile = user.json_import()
            return user
        except ConnectionRefusedError:
            sys.exit("Too many requests. Try again later")
        except ConnectionError:
            print("user not found. try again.")
            continue


def highest_karma(first_user, second_user):
    """compares karma from users 1 and 2's most recent post/comment and evaluates which one
       is higher.
    """
    global submission_type

    if first_user.karma > second_user.karma:
        print("{}'s most recent {} has the highest Karma, with {} points.".format(
            second_user.username, submission_type, first_user.karma))
    elif second_user.karma > first_user.karma:
        print("{}'s most recent {} has the highest Karma, with {} points.".format(
            second_user.username, submission_type, second_user.karma))
    elif second_user.karma == first_user.karma:
        print("{} and {}'s most recent {} have the same karma, with {} points.".format(
            first_user.username, second_user.username, submission_type, second_user.karma))


if __name__ == '__main__':
    main()
